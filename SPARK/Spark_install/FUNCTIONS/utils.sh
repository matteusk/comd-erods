#/!/bin/bash

##############################################################

local_connection (){
	#ssh-keygen -t rsa
	cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
	chmod og-wx ~/.ssh/authorized_keys 	
}

nodefile_verification (){

nodefile=$1
dir=$2

if [ -f $pwd$nodefile ]
then
        echo "Loading nodefile: $nodefile..." 
   else
	echo "$nodefile not found..."
	echo "Please, create a nodefile list!"
	exit 0
fi

[ ! -s $nodefile ] && echo "The nodefile file is empty..." && exit 0

}

###############################################################

#this function can be used if there is some problem of the key in known_hosts
update-local-key () {
  localuser=$USER
  nodefile=$1
  dir=$2
  echo "updating keys from local node to the allocated ones"
  for i in `cat $dir/$nodefile`;
  do	
	ssh-keygen -f "/home/$localuser/.ssh/known_hosts" -R $i 
  done
}

###############################################################

exchange-keys(){
  nodefile=$1
  dir=$2
  user=$3

  update-local-key $nodefile $dir
  config="~/ERODS/Scripts/FUNCTIONS/ssh_config"
  #creating folder for the key
  if [ ! -d "key" ];then
	mkdir -p $dir/key
  fi
  
  for i in `cat $dir/$nodefile`;
  do
	#creating keys overall nodes
	ssh $user@$i "if [ -f ~/.ssh/id_rsa ]; then mv ~/.ssh/id_rsa ~/.ssh/id_rsa_old; ssh-keygen -N '' -t rsa -f ~/.ssh/id_rsa; else ssh-keygen -N '' -t rsa -f ~/.ssh/id_rsa; fi"
  	
  done


  for i in `cat $dir/$nodefile`;
  do
    CNode=$i
    cat ~/ERODS/Scripts/FUNCTIONS/ssh_config | ssh $user@$i "cat >> ~/.ssh/config"
    for probe in `cat $dir/$nodefile`;
    do
	rm -rf $dir/key/*
	#excluding current node from itsel key copy
	if [ "$probe" != "$CNode" ]; then
		echo "Copying key from $CNode to $probe"
		scp -r $user@$CNode:/$user/.ssh/id_rsa.pub $dir/key/
		cat $dir/key/id_rsa.pub | ssh $user@$probe "cat >> ~/.ssh/authorized_keys"
	fi
    done
  done
  rm -rf $dir/key

}

discovery-cluster(){
	nodefile=$1
	echo $nodefile "--"
	> $nodefile
	for i in `uniq $OAR_NODE_FILE`;
	do
		echo $i >> $nodefile
	done
}
