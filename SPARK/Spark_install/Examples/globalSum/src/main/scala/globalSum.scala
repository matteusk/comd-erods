package globalsum

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming._
import org.apache.spark.storage.StorageLevel

object GlobalSum {

    def reduce_batch(x: (Int, Double), y: (Int, Double)): (Int, Double) = {
	(x._1 + y._1, x._2 + y._2)
    }

    def aggregate_vals(newValues: Seq[(Int, Double)], runningCount: Option[(Int,Double)]):Option[(Int,Double)] = {
        val prevCount = runningCount.getOrElse((0, 0.0))
        var currentCount = (0, 0.0)
        if(newValues.size > 0){
            currentCount = newValues.reduce((x, y) => (x._1 + y._1, x._2 + y._2))    
        }
        val newCount = (prevCount._1 + currentCount._1, prevCount._2 + currentCount._2)
        Some(newCount)
    }

    def add_values(key: String, batchSums: Option[(Int, Double)], state: State[(Int, Double)]): (String,(Int,Double)) = {
        (batchSums, state.getOption()) match {
            case (Some(newSum), None) => {
                // there is no state for this key
                val new_state = (newSum._1, newSum._2)
                state.update(new_state)
                (key, new_state)
            }
            case (Some(newSum), Some(prevState)) => {
                // There is a state about this key, and we have new values
                val new_state = (newSum._1 + prevState._1, newSum._2 + prevState._2)
                state.update(new_state)
                (key, new_state)
            }
            case (None, Some(prevState)) => {
                // We have not recieved new data, just return the previous one
                (key, prevState)
            }
            case _ => ("",(0,0)) //We should never reach this case
        }
    }


    def main(args: Array[String]): Unit = {

        val trigger = 10000
        // start spark 
        //.setMaster("local[*]")
        val conf = new SparkConf().setAppName("globalSum")
        val sc = new SparkContext(conf)
        sc.setLogLevel("ERROR")
        val ssc = new StreamingContext(sc, Seconds(2))

        // needed when using state
        ssc.checkpoint("../checkpoint_GlobalSum")

        // Get Data
        val dataStream = ssc.socketTextStream("127.0.0.1",9009)
	//val partitionedStream = dataStream.repartition(25)


        // Divide the input in (step_id, value) pairs
        //val dataString = dataStream.flatMap(_.split(" "))

        // Create the data (step_id, <nb_values, sum_values>), where step_id is treated as key
        val data = dataStream.map(x => (x.split(",")(0), (1, x.split(",")(1).toDouble)))
        
        // Sum the <nb_values, sum_values> tuples using <step_id> as key
        val batchSum = data.reduceByKey(reduce_batch(_,_), 16)
        batchSum.print()

        // Filter the data that has not arrived complte
        val notCompleted = batchSum.filter(_._2._1 < trigger)
        //notCompleted.persist(StorageLevel.MEMORY_ONLY_SER )

        // Get the data that has arrived complete
        val completed = batchSum.filter(_._2._1 == trigger)

        
        // Add the current batch sums with the state, only if we do not have recieved all data
        // Note: Not using Some/None Scala feature (Should we?)
        val total = notCompleted.mapWithState(StateSpec.function(add_values _).numPartitions(16))
        // val state = total.stateSnapshots()
        //total.persist(StorageLevel.MEMORY_ONLY )
        //total.checkpoint(Seconds(3))
        //val total = data.updateStateByKey(aggregate_vals(_,_), 1)
        //total.print()
        

        // Group all data
        val allData = completed.union(total)

        //Compute and display all the results
        val out = allData.map(x => {
                val step  = x._1.toInt
                val count = x._2._1
                val sum   = x._2._2
                val result = sum / (trigger*step) //The result of the completed results should be always 1
                if(count == trigger)
                    (step, result)
                else
                    (-1, -1)
        })
        
        val toPrint = out.filter(_._1 >= 0)
        toPrint.print()

	/*
        //Compute and display all the results
        allData.foreachRDD(rdd => {
            val out = rdd.map(x => {
                val step  = x._1.toInt
                val count = x._2._1
                val sum   = x._2._2
                val result = sum / (trigger*step) //The result of the completed results should be always 1
                if(count == trigger)
                    //println("*Step* " + step + ": " + result)
                    printf("")
                else 
                    //println("Step " + step + ": " + result)
                    printf("")
            }).collect()
        })

        val numRDDs = dataString.count()
        val throughput = numRDDs.map(x=> x / 10000)
        throughput.print()
	*/
        
        ssc.start()
        ssc.awaitTermination()
    }
}

//NOTE: Not using entries.cache()!!!
