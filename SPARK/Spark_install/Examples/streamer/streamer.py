import socket
import sys
import time

num_values_creted_per_step = 0
current_step = 0
last_i = 0
data = []

#Create a chunk of data to be streamed
def create_chunk(chunk_size, repetition, step_size):
        
        global num_values_creted_per_step
        global current_step
        global last_i
        global data

        for i in range(chunk_size):
               i += last_i
               
               step_id = i%repetition + current_step
               if step_id == current_step and num_values_creted_per_step == step_size:
                     current_step += (i%repetition + repetition)
                     step_id = i%repetition + current_step
                     if i%repetition == 0:
                          num_values_creted_per_step = 0

               value = step_id + 1
               data[i-last_i] = (step_id + 1, value)

               if step_id == current_step:
                     num_values_creted_per_step += 1

        last_i = step_id%repetition + 1
        


def send_stream_to_spark(message, tcp_connection):
        try:
               #data = message + '\n'
               data = message
               #print("Sream: " + data)
               #print ("------------------------------------------")
               data_bytes = data.encode("utf-8")
               tcp_connection.send(data_bytes)
        except:
               e = sys.exc_info()[1]
               print("Error Here: %s" % e)


def main(argv):

        global data

        if len(argv) < 5:
               print("Usage: streamer.py <chunk_size> <repetition> <step_size> <period(s)>")
               return

        chunk_size = int(argv[1])
        repetition = int(argv[2])
        step_size = int(argv[3])
        period = float(argv[4])
        
        print('chunk_size %d, repetition %d, step_size %d, period(s) %f' % (chunk_size, repetition, step_size, period))

        data = [None]*chunk_size


        TCP_IP = "127.0.0.1"
        TCP_PORT = 9009
        conn = None
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((TCP_IP, TCP_PORT))
        s.listen(1)
        print("Waiting for TCP connection...")
        conn, addr = s.accept()
        print("Connected... Starting sending stream.")
        loop_time_prev = time.time()
        while (True):
               # With python3 streamerV3.py 200 1000 10000 0, creating data takes ~ 0.00017 and completing the loop ~0.0002
               create_chunk(chunk_size, repetition, step_size)
               data_stream = ""
               for d in data:
                     data_stream += str(d[0]) + "," + str(d[1]) + "\n"
               send_stream_to_spark(data_stream, conn)
               time.sleep(period)

               loop_time_act = time.time()
               loop_time = loop_time_act - loop_time_prev
               loop_time_prev = loop_time_act
               print("real delay: " + str(loop_time) + " pairs/sec: " + str(chunk_size/loop_time))


if __name__ == '__main__':
    main(sys.argv)


#https://stackoverflow.com/questions/47391774/python-send-and-receive-objects-through-sockets
