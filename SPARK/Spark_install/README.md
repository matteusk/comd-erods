This script installs Apache Spark, java and Scala in local mode.

To configure the installer, you need to change the following values at CONFIG/conf.DATA

user="your user"
installpath="path to install"

Obs. Ensure full permission for the selected folder

Also, you need to provide an IP address (e.g, 127.0.0.1) for your Spark node into a file

listofsparknodes="filename"

To run:

./SparkLocal.sh
