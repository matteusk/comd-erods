#/!/bin/bash
#	title           :Deploy Spark in local and cluster mode
#	description     :Install spark-2.4.3-bin-hadoop2.7.tgz, scala-2.13.0.tgz and, jdk-8u131-linux-x64.tar.gz
#	author			:Kassiano Jose Matteussi "kjmatteussi@inf.ufrgs.br"
#	date            :Tue Set 19 11:31:29 CEST 2019
#	version         :1.0
#	usage			:deploy-spark.sh


#### Global Parameters and Variables ###
source FUNCTIONS/utils.sh
source CONFIG/conf.DATA
dir=$(pwd)
#mkdir -p $installpath
#mkdir -p /tmp$installpath

master=`cat $dir/$listofsparknodes | head -n1`

PK_PATH="$installpath/spark"
SCL_PATH="$installpath/Scala" 
J_PATH="$installpath/java"
D_NODE="$installpath/data/dataNode"
N_NODE="$installpath/data/nameNode"


HADOOP_PATH="$installpath/hadoop"

quote="/"
lib="/usr/local/lib"
jeromq="/home/kassiano/Documents/gitRep/comd-erods/MQ/jzmq-master/jzmq-jni/src/main/c++/.libs/"
javajar="/usr/local/share/java/zmq.jar"


##### ##### ##### ##### ##### ##### ##### 
start(){
	echo "Deploying the cluster..."
	for i in `cat $dir/$listofsparknodes`; 
    do 
		echo "Creating environment in "$i 
		echo "Copying files and setting profile files..."
		scp -r SoftwareStack/* $user@$i:$installpath
		ssh $user@$i "				 
				for tarbal in \`ls $installpath\`; do tar -xzf $installpath/\$tarbal -C $installpath; done &>/dev/null
				mkdir -p $PK_PATH $SCL_PATH $J_PATH $HADOOP_PATH $D_NODE $N_NODE /tmp/spark-events   
				mv $installpath/jdk1.8.0_131/* $J_PATH$quote
				mv $installpath/scala-2.13.0/* $SCL_PATH$quote
				mv $installpath/spark-2.4.3-bin-hadoop2.7/* $PK_PATH$quote
				mv $installpath/hadoop-3.1.2/* $HADOOP_PATH$quote
				echo \"export JAVA_HOME=$J_PATH\" > ~/.bashrc
                echo \"export SCALA_HOME=$SCL_PATH\" >> ~/.bashrc      
				echo \"export SPARK_HOME=$PK_PATH\" >> ~/.bashrc
				echo \"export HADOOP_HOME=$HADOOP_PATH\" >> ~/.bashrc
				echo \"export LIB=$lib\" >> ~/.bashrc
				echo \"export JEROMQ=$jeromq\" >> ~/.bashrc
				echo \"export JAVAJAR=$javajar\" >> ~/.bashrc"
        ssh $user@$i '
				echo "export PATH=\$JAVA_HOME/bin:\$PATH" >> ~/.bashrc 
				echo "export PATH=\$SCALA_HOME/bin:\$PATH" >> ~/.bashrc
				echo "export PATH=\$SPARK_HOME/sbin:\$PATH" >> ~/.bashrc
				echo "export PATH=\$SPARK_HOME/bin:\$PATH" >> ~/.bashrc
				echo "export PATH=\$HADOOP_HOME/sbin:\$PATH" >> ~/.bashrc
				echo "export PATH=\$HADOOP_HOME/bin:\$PATH" >> ~/.bashrc
				echo "export PATH=\$LIB:\$PATH" >> ~/.bashrc
				echo "export PATH=\$JEROMQ:\$PATH" >> ~/.bashrc
				echo "export LD_LIBRARY_PATH=\$LIB:\$LD_LIBRARY_PATH" >> ~/.bashrc
				echo "export LD_LIBRARY_PATH=\$JEROMQ:\$LD_LIBRARY_PATH" >> ~/.bashrc
				echo "export CLASSPATH=\$JAVAJAR:\$CLASSPATH" >> ~/.bashrc
				echo "export HDFS_NAMENODE_USER=\$user" >> ~/.bashrc
				echo "export HDFS_DATANODE_USER=\$user" >> ~/.bashrc
				echo "export HDFS_SECONDARYNAMENODE_USER=\$user" >> ~/.bashrc
				'
				
				source ~/.bashrc
				echo "ok"
				echo "export SPARK_MASTER_HOST='$master'" > Conf_Files/sp/spark-env.sh
				echo $i >> Conf_Files/sp/slaves
				echo $i >> Conf_Files/had/workers
				
			

				datanode=$(echo "$D_NODE" | sed 's/\//\\\//g')
				namenode=$(echo "$N_NODE" | sed 's/\//\\\//g')
			
				sed  "s/\${NAMENODE}/${namenode}/g; s/\${DATANODE}/${datanode}/g" Conf_Files/hdfs-site.xml > Conf_Files/had/hdfs-site.xml
				sed  s/\${HDFS_MASTER}/${master}/g Conf_Files/core-site.xml > Conf_Files/had/core-site.xml
				
				
				
				
				
	done
    echo "Starting the cluster..."
    scp Conf_Files/sp/* $user@$master:$installpath/spark/conf/
    scp Conf_Files/had/* $user@$master:$installpath/hadoop/etc/hadoop/
    echo "Starting SPARK"
    ssh $user@$master  'source ~/.bashrc; start-master.sh; sleep 2; start-slaves.sh; sleep 1; start-history-server.sh '
    echo "Starting HADOOP"
    ssh $user@$master  'source ~/.bashrc; hdfs namenode -format; start-dfs.sh'
    echo "creating output folder $outputfolder in HDFS"
    ssh $user@$master  "hdfs dfs -mkdir -p $outputfolder"

    

    echo "Done!"
}

stop()
{
	echo "Stopping services and server side cleaning ..."

	for i in `cat $dir/$listofsparknodes`; 
	do 
	   #if [ "$i" == "$master" ]; then
		   ssh $user@$master  "stop-history-server.sh; stop-slaves.sh; stop-master.sh; stop-dfs.sh; echo \"Done! \""
	   #fi
	   echo "Cleaning server $i"
	   ssh $user@$i " [ -d /tmp/hadoop-$user/ ] && rm -rf /tmp/hadoop-$user/ || echo \"Directory /tmp/hadoop-$user/  not found\" "
	   ssh $user@$i " [ -d /tmp/spark-events/ ] && rm -rf /tmp/spark-events/ || echo \"Directory /tmp/spark-events/ not found\" "
	   ssh $user@$i " [ -d $installpath/data/ ] && rm -rf $installpath/data/ || echo \"Directories of Nade and data node ($installpath/data/) was not found\" "
	   ssh $user@$i " [ -d $PK_PATH ] && rm -rf $PK_PATH || echo \"Directory $PK_PATH not found\" "
	   ssh $user@$i " [ -d $SCL_PATH ] && rm -rf $SCL_PATH || echo \"Directory $SCL_PATH not found\" "
	   ssh $user@$i " [ -d $J_PATH ] && rm -rf $J_PATH || echo \"Directory $J_PATH not found\" "
	   ssh $user@$i " [ -d $HADOOP_PATH ] && rm -rf $HADOOP_PATH || echo \"Directory $HADOOP_PATH not found\" "

	   done
	   echo "Cleaning conf files..."
	   [ -e Conf_Files/sp/slaves ] &&  echo " " > Conf_Files/sp/slaves || echo "File Not found!!"
	   [ -e Conf_Files/had/workers ] &&  echo " " > Conf_Files/had/workers || echo "File Not found!!"


}

batch()
{
	echo "Running an Example of Batch application "
	ssh $user@$master "spark-submit --master spark://$master:7077 $installpath/spark/examples/src/main/python/pi.py 200"
}

stream_stop()
{
	rm -rf Examples/checkpoint_GlobalSum/
	rm -rf Examples/globalSum/target

	echo " Python proccess running!! "
	ps aux | grep python | grep -v "grep python" | awk '{print $2}'
	echo " Spark proccess running!! " 
	ps aux | grep SparkSubmit | grep -v "grep SparkSubmit" | awk '{print $2 }' 
	echo " Time to kill!! -> Python"
	while true
	do
		a=`ps -aux | grep python | grep -v "grep python" | awk '{print $2}'`
		b=`ps -aux | grep SparkSubmit | grep -v "grep SparkSubmit" | awk '{print $2}'` 
		sudo kill -9 $a $b 2> /dev/null || return 0	
	done
}

stream()
{	
	#todo - function to finish de processors overall nodes, send modified files based on the master node, perl script to adjust identation of python script, readme file 
	echo "Starting GlobalSum Streaming Application. "
	cd Examples/globalSum/
	sbt package
	cd ../streamer/
	sudo python3 streamer.py 500000 500000 10000 4.9 $master&
	sleep 2
	source ~/.bashrc
	spark-submit --master spark://$master:7077 ../globalSum/target/scala-2.11/globalsum_2.11-1.0.jar 	
}

GUI()
{
	echo "WebUI Spark          --> http://$master:8080"
	echo "AppUI Spark          --> http://$master:4040 (only during job execution)"
	echo "History Server Spark --> http://$master:18080 (run the program with the history log option: --conf spark.eventLog.enabled=true )"
	echo "WebUI HDFS           --> http://$master:9870 "
	echo "HDFS Commands:"
	echo "	hdfs dfs -ls hdfs://$master:9000/<outout folder created> "
	echo "Running an application:"
	echo "	spark-submit --master spark://$master:7077 --class StatelessSUMServer target/simple-project-1.0-jar-with-dependencies.jar --PORT 4040 --NETWORK enxa44cc8f082ca --DATASOURCES 127.0.0.1 --TEMPON 10000 --RECEIVERS 1 --CLIENTS 1 --WINDOWTIME 1000 --BLOCKINTERVAL 200 --CONCBLOCK 1 --HDFS  hdfs://$master:9000/<folder> --PARAL 8"
	
}

help()
{
cat << EOF

This script  - ./SparkLocal.sh - spark-2.4.3-bin-hadoop2.7.tgz, scala-2.13.0.tgz and, jdk-8u131-linux-x64.tar.gz
Just run, enjoy!
 
USAGE:  ./SparkLocal.sh [option]
 
OPTIONS:
   -h,  --help            Time to coffe! 
   -s,  --start           Deploy Spark in local mode
   -p,  --stop            Stop all Spark components and Workers, removing all associated data.
   -b,  --batch           Run Pi batch application 
   -t,  --stream          Run Globalsum Streaming application (generates and consume data in a local mode).
   
EXAMPLES: 
     ./SparkLocal.sh -s
     ./SparkLocal.sh --start
             
EOF
}

while true;
do
  case "$1" in

    -h|--help)
      help
      exit 0
      ;;
    -s|--start)
		nodefile_verification $listofsparknodes $dir
		local_connection
		stop
		start
		GUI
      break
      ;;
    -p|--stop)
    	nodefile_verification $listofsparknodes $dir
		stop
      break
      ;;
    -b|--batch)
		GUI
		batch
      break
      ;;
    -t|--stream)
		#TODO - set up ip parameter in both applications, add script to kill Apps' process and clean folders.
		stream 
      break
      ;;
    --|-|*)
      help
      break
      ;;
  esac
done


