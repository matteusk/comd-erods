#!/bin/bash

#normal
#spark-submit --master spark://127.0.0.1:7077 ~/Documents/gitRep/comd-erods/SPARK/Spark_install/Examples/globalSum/target/scala-2.11/globalsum_2.11-1.0.jar

#BP Oonly
#spark-submit --master spark://127.0.0.1:7077 --conf spark.streaming.backpressure.enabled=true ~/Documents/gitRep/comd-erods/SPARK/Spark_install/Examples/globalSum/target/scala-2.11/globalsum_2.11-1.0.jar

#BP + MAX rec

spark-submit --master spark://127.0.0.1:7077 --conf spark.streaming.backpressure.enabled=true --conf spark.streaming.receiver.maxRate=200 ~/Documents/gitRep/comd-erods/SPARK/Spark_install/Examples/globalSum/target/scala-2.11/globalsum_2.11-1.0.jar


#spark-submit --class StatelessSUMServer --master spark://127.0.0.1:7077 --conf spark.executor.extraJavaOptions="-XX:+UseG1GC" --conf spark.streaming.backpressure.enabled=true --conf spark.streaming.backpressure.initialRate=5 --conf spark.streaming.backpressure.pid.minRate=10 --conf spark.streaming.receiver.initialRate=5 --conf spark.streaming.receiver.maxRate=20 --conf spark.streaming.backpressure.pid.proportional=1 --conf spark.streaming.backpressure.pid.derived=0.2 --conf spark.streaming.backpressure.pid.integral=0.2 target/simple-project-1.0-jar-with-dependencies.jar --THRESHOLD 1000 --NETWORK wlp2s0 --DATASOURCES 127.0.0.1 --TEMPON $(((10000-8)/4)) --RECEIVERS 4 --CLIENTS 4 --WINDOWTIME 2000 --BLOCKINTERVAL 400 --CONCBLOCK 1 --HDFS hdfs://127.0.0.1:9000/StatelessSUMServer --PARAL 8



#spark-submit --class StatelessSUMServer --master spark://127.0.0.1:7077 --conf spark.executor.extraJavaOptions="-XX:+UseG1GC" --conf spark.streaming.backpressure.enabled=true --conf spark.streaming.backpressure.pid.minRate=5 --conf spark.streaming.backpressure.pid.proportional=1 --conf spark.streaming.backpressure.pid.derived=0.5 --conf spark.streaming.backpressure.pid.integral=0.2 --conf spark.streaming.receiver.initialRate=15 --conf spark.streaming.receiver.maxRate=400 target/simple-project-1.0-jar-with-dependencies.jar --THRESHOLD 1000 --NETWORK wlp2s0 --DATASOURCES 127.0.0.1 --TEMPON $(((10000-8)/4)) --RECEIVERS 4 --CLIENTS 4 --WINDOWTIME 2000 --BLOCKINTERVAL 400 --CONCBLOCK 1 --HDFS hdfs://127.0.0.1:9000/StatelessSUMServer --PARAL 8


#--conf spark.streaming.backpressure.rateEstimator=pid \
#--conf spark.streaming.backpressure.initialRate=1000 \
#--conf spark.streaming.backpressure.pid.minRate=1000 \
#--conf spark.streaming.backpressure.pid.integral=1 \
#--conf spark.streaming.backpressure.pid.proportional=25 \
#--conf spark.streaming.backpressure.pid.derived=1 \


