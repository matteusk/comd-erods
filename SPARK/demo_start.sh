#!/bin/bash

#hadoop fs -rm -r /StatelessSUMServer
#hdfs dfs -mkdir -p /StatelessSUMServer

#spark-submit --class StatelessSUMServer --master spark://127.0.0.1:7077  --conf spark.driver.memory="2g" --conf park.executor.memory="4g"  --conf spark.executor.extraJavaOptions="-XX:+UseG1GC" --conf spark.streaming.backpressure.enabled=true --conf spark.streaming.backpressure.initialRate=100 target/simple-project-1.0-jar-with-dependencies.jar --THRESHOLD 1000 --NETWORK wlp2s0 --DATASOURCES 127.0.0.1 --TEMPON $(((10000-8)/4)) --RECEIVERS 4 --CLIENTS 4 --WINDOWTIME 2000 --BLOCKINTERVAL 400 --CONCBLOCK 1 --HDFS hdfs://127.0.0.1:9000/StatelessSUMServer --PARAL 8


hadoop fs -rm -r /SUMServer
hdfs dfs -mkdir -p /SUMServer


spark-submit --class SUMServer --master spark://127.0.0.1:7077  --conf spark.driver.memory="2g" --conf spark.executor.memory="4g"  --conf spark.executor.extraJavaOptions="-XX:+UseG1GC" --conf spark.streaming.backpressure.enabled=true --conf spark.streaming.backpressure.initialRate=100 target/simple-project-1.0-jar-with-dependencies.jar --THRESHOLD 1000 --NETWORK wlp2s0 --DATASOURCES 127.0.0.1 --TEMPON 10000 --RECEIVERS 2 --CLIENTS 4 --WINDOWTIME 2000 --BLOCKINTERVAL 400 --CONCBLOCK 1 --HDFS hdfs://127.0.0.1:9000/SUMServer --PARAL 8



