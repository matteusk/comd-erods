#ifndef EREBOR
#define EREBOR

typedef struct erebor{
    char *rank;
    char *network;
    void *context;
    void *puller;
    void *pusher;
    char *buff;
} erebor;

/*
 * Initialize a connection to an erebor daemon.
 * str_rank is the MPI rank of this core
 * network  if the name of the group where this mpi_job is launched
 * in_ipc   is the name of the file to connect to the erebor daemon
 * out_ipc  is the name that will be given to the daemon in order to contact us.
 * erebor   is a pointer to an allocated erebor struct
 *
 * char* are copyed into the erebor data structure.
 * They can be freed after this function call
 * return -1 in case of error, erebor_print_error will give more indication
 */
int erebor_init_connection(char* str_rank, char* network, char* in_ipc, char* out_ipc, erebor* erebor);

/*
 * Send a message to a foreign MPI rank on a foreign group
 * erebor  is a pointer to an initialized erebor struct
 * dest    is the MPI rank of the receiver
 * group   is the group name of the receiver
 *
 * char* can be freed after this function call
 * return > 0  if a message sent
 * return -1 for any other error, erebor_print_error will give more indication
 */
int erebor_send_to(erebor* erebor, char* dest, char* group, char* message);

/*
 * Send a message to a foreign MPI rank on a foreign group
 * erebor  is a pointer to an initialized erebor struct
 * dest    is the MPI rank of the receiver
 * group   is the group name of the receiver
 *
 * char* can be freed after this function call
 * return > 0  if a message sent
 * return -2 if sending impossible
 * return -1 for any other error, erebor_print_error will give more indication
 */
int erebor_non_block_send_to(erebor* erebor, char* dest, char* group, char* message);

/*
 * Send a control message to an Erebor instance
 * erebor  is a pointer to an initialized erebor struct
 * group   is the group name of Erebor instance
 *
 * char* can be freed after this function call
 * return > 0  if a message sent
 * return -1 for any other error, erebor_print_error will give more indication
 */
int erebor_ctrl_msg_to(erebor* erebor, char* group, char* message);

/*
 * Send a control message to an Erebor instance
 * erebor  is a pointer to an initialized erebor struct
 * group   is the group name of Erebor instance
 *
 * char* can be freed after this function call
 * return > 0  if a message sent
 * return -2 if sending impossible
 * return -1 for any other error, erebor_print_error will give more indication
 */
int erebor_non_block_ctrl_msg_to(erebor* erebor, char* group, char* message);

/*
 * Receive a message form erebor daemon
 * erebor  is a pointer to an initialized erebor struct
 * dest    preallocated buffer where the MPI rank of the sender will be written
 * group   preallocated buffer where the group of the sender will be written
 * message preallocated buffer where the content of the message will be written
 *
 * Always return null terminated strings, message is truncated.
 *
 * return 0 if everything is fine
 * return -1 otherwise, erebor_print_error will give more indication
 */
int erebor_recv(erebor* erebor, char*dest, char*group, char*message);

/*
 * NON blocking receive
 * erebor  is a pointer to an initialized erebor struct
 * dest    preallocated buffer where the MPI rank of the sender will be written
 * group   preallocated buffer where the group of the sender will be written
 * message preallocated buffer where the content of the message will be written
 *
 * return 0  if a message has arrived
 * return -2 if no message was there
 * return -1 for any other error, erebor_print_error will give more indication
 */
int erebor_non_block_recv(erebor* erebor, char*dest, char*group, char*message);

/*
 * Close the connections.
 * erebor   is a pointer to an initialized erebor struct
 *
 * erebor data structure need to be freed afterward.
 */
void erebor_close(erebor* erebor);

/*
 * Print a human readable error message from errno (from zmq errors)
 */
void erebor_print_error();

#endif
