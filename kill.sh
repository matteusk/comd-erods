#!/bin/bash


while true
do
	kill -9 $(pidof stencyl message_queue  message_queue_WSS demo) 2> /dev/null && echo MPI and MQ were closed || echo Nothing to do! && break 
done


echo "--------SPARK--------"

#spark=`cat $folder/$listofsparknodes |head -1`

for app in StatelessSUMServer SUMServer
do
	echo "Trying to close $app ..."
	echo 'Proc to kill:'; ps ax | grep -v grep | grep -e "$app" | awk '{print $1}';kill -9 $(ps ax | grep -v grep | grep -e "$app" | awk '{print $1}') 2> /dev/null
done


#while true
#do
#	for i in StatelessSUMServer SUMServer
#	do
#		echo "Trying to close $i ..."
#		aux=`ps -eF |grep -v grep |grep -e $i |cut -d " " -f 2`
#		echo $aux
#		kill -9 $aux 2> /dev/null && echo Streaming $i application was closed! || echo Nothing more to do... 
#
#	done
#	break

#done
